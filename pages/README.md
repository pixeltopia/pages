#  Pages

This is a demo app that renders components.

## Icons

The app icon has been created with the following tools:

- [Opacity] (http://likethought.com/opacity/)
- [IconFly] (https://iconfly.aperio-lux.com)
