import UIKit

@UIApplicationMain
class App: UIResponder, UIApplicationDelegate {
    typealias LaunchOptions = [UIApplication.LaunchOptionsKey: Any]

    var window: UIWindow?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: LaunchOptions?
    ) -> Bool {
        true
    }
}

class AppController: ViewController {
    override var createPresenter: Presenter? {
        AppPresenter()
    }
}

class AppPresenter: Presenter {
    func receive(_ view: View, _ intent: Intent) {
        switch intent {
            case .ready:
                view.plugins[.text]  = TextPlugin()
                view.plugins[.image] = ImagePlugin()
                view.render(model())
        }
    }

    func model() -> Model {
        Model(
            Item(.text(Text(value: "Lorem ipsum dolor sit amet"))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet"))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."))),
            Item(.image(Image(value: "https://placekitten.com/320/180".toURL))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet"))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet"))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."))),
            Item(.image(Image(value: "https://placekitten.com/640/640".toURL))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet"))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet"))),
            Item(.text(Text(value: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")))
        )
    }
}

// MARK: - MVP

enum Intent {
    case ready
}

protocol View: class {
    var plugins: Plugins { get set }
    func render(_ model: Model)
}

protocol Presenter: class {
    func receive(_ view: View, _ intent: Intent)
}

// MARK: - implementation

class ViewController: UIViewController, View {

    private enum Section: Hashable {
        case main
    }

    private struct Cell: Hashable {
        let item:    Item
        let version: Plugin.Version

        init(_ item: Item) {
            self.item    = item
            self.version = 1
        }

        init(_ item: Item, _ version: Int) {
            self.item    = item
            self.version = version
        }
    }

    private var table:     UITableView?
    private var cells:     [Cell]?
    private var diffs:     UITableViewDiffableDataSource<Section, Cell>?
    private var presenter: Presenter?

    var createPresenter: Presenter? { nil }

    var plugins = Plugins()

    final override func viewDidLoad() {
        super.viewDidLoad()

        let table                = UITableView()
        table.backgroundView     = nil
        table.backgroundColor    = .clear
        table.separatorStyle     = .none
        table.rowHeight          = UITableView.automaticDimension
        table.estimatedRowHeight = 400 // arbitrary

        diffs = UITableViewDiffableDataSource<Section, Cell>(tableView: table) { (table, path, cell) -> UITableViewCell? in
            let item       = cell.item
            let version    = cell.version
            let identifier = item.data.kind.rawValue

            let container = table.dequeueReusableCell(withIdentifier: identifier) ??
                UITableViewCell(style: .default, reuseIdentifier: identifier)

            if let view = container.contentView.subviews.first ?? self.plugins.view(item) {
                self.plugins.bind(item, view, version) { [weak self] version in
                    print("C -> \(version)")
                    self?.invalidate(Cell(item, version), at: path)
                }
                container.contentView.addSubview(view, container.contentView.contains(view))
            }

            return container
        }

        table.dataSource = diffs

        view.addSubview(table, view.safeArea(table))

        presenter = createPresenter

        self.table = table
    }

    func render(_ model: Model) {
        cells = plugins.remove(unsupported: model).items.map(Cell.init)
        apply()
    }

    private func invalidate(_ cell: Cell, at indexPath: IndexPath) {
        cells?[indexPath.item] = cell
        apply()
    }

    private func apply(animated: Bool = false) {
        var diff = NSDiffableDataSourceSnapshot<Section, Cell>()
        diff.appendSections([.main])
        diff.appendItems(cells ?? [])
        diffs?.apply(diff, animatingDifferences: animated, completion: nil)
    }

    final override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.receive(self, .ready)
    }

    override func willTransition(
        to newCollection: UITraitCollection,
        with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        table?.reloadData()
    }
}

// MARK: - view

protocol Plugin {
    typealias Version    = Int
    typealias Invalidate = (Version) -> Void
    func view(_ data: Item.Data) -> UIView
    func bind(_ data: Item.Data, _ view: UIView, _ version: Version, _ invalidate: @escaping Invalidate)
}

// MARK: - views

extension UIView {
    func safeArea(_ view: UIView) -> [NSLayoutConstraint] {
        return [
            view.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            view.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            view.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor),
            view.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor),
        ]
    }

    func contains(_ view: UIView) -> [NSLayoutConstraint] {
        return [
            view.topAnchor.constraint(equalTo: topAnchor),
            view.bottomAnchor.constraint(equalTo: bottomAnchor),
            view.leftAnchor.constraint(equalTo: leftAnchor),
            view.rightAnchor.constraint(equalTo: rightAnchor),
        ]
    }

    func addSubview(_ view: UIView, _ constraints: [NSLayoutConstraint]) {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        addConstraints(constraints)
        NSLayoutConstraint.activate(constraints)
    }
}

extension UIView {
    func animate(
        _ begin:    @autoclosure () -> Void = {}(),
        animations: @escaping () -> Void,
        completion: ((Bool) -> Void)? = nil) {
        begin()
        UIView.transition(
            with: self,
            duration: 0.25,
            options: .transitionCrossDissolve,
            animations: animations,
            completion: completion)
    }
}

// MARK: - plugin

struct Plugins {
    private var plugins = [Item.Data.Kind: Plugin]()

    subscript(_ kind: Item.Data.Kind) -> Plugin? {
        get { plugins[kind] }
        set { plugins[kind] = newValue }
    }

    func remove(unsupported model: Model) -> Model {
        Model(model.items.filter { plugins.keys.contains($0.data.kind) })
    }

    func view(_ item: Item) -> UIView? {
        self[item.data.kind]?.view(item.data)
    }

    func bind(
        _ item: Item,
        _ view: UIView,
        _ version: Plugin.Version,
        _ invalidate: @escaping Plugin.Invalidate) {
        self[item.data.kind]?.bind(item.data, view, version, invalidate)
    }
}

// MARK: - models

struct Model {
    let items: [Item]

    init(_ items: [Item]) {
        self.items = items
    }

    init(_ items: Item...) {
        self.items = items
    }
}

struct Item: Hashable {
    typealias Name = UUID

    // MARK: - this needs to be extended with new cases/kinds

    enum Data: Hashable {
        enum Kind: String {
            case text
            case image
        }

        var kind: Kind {
            switch self {
                case .text:  return .text
                case .image: return .image
            }
        }

        case text(Text)
        case image(Image)
    }

    let name: Name
    let data: Data

    init(_ data: Data) {
        self.name = UUID()
        self.data = data
    }

    init(_ item: Item) {
        self.name = UUID()
        self.data = item.data
    }
}

// MARK: - models - kinds

struct Text: Hashable {
    let value: String
}

struct Image: Hashable {
    let value: URL
}

// MARK: - Helpers

protocol Tappable: AnyObject {}

extension Tappable {
    @discardableResult func tap(block: (Self) -> Void) -> Self {
        block(self)
        return self
    }
}

extension UIView: Tappable {}
extension NSLayoutConstraint: Tappable {}

extension String {
    var toURL: URL { URL(string: self)! }
}

extension CGSize {
    var aspectRatio: CGFloat { width / height }
}

extension UIView {
    static func nibbed<T: UIView>(
        _ bundle: Bundle = .main,
        _ nibName: String = "Views",
        _ type: T.Type = T.self) -> T? {
        guard let items = bundle.loadNibNamed(nibName, owner: nil, options: nil)
            else { return nil }
        return items.first { $0 is T } as? T
    }
}

// MARK: - Plugins

struct TextPlugin: Plugin {

    func view(_ data: Item.Data) -> UIView {
        UITextView().tap {
            $0.isScrollEnabled = false
            $0.isEditable = false
            $0.font = .preferredFont(forTextStyle: .body)
            $0.backgroundColor = .systemBackground
            $0.textColor = .label
        }
    }

    func bind(_ data: Item.Data, _ view: UIView, _ version: Version, _ invalidate: @escaping Invalidate) {
        guard case let .text(text) = data, let view = view as? UITextView else { return }
        view.text = text.value
        view.sizeToFit()
        view.animate(view.alpha = 0) {
            view.alpha = 1
        }
    }
}

struct ImagePlugin: Plugin {

    func view(_ data: Item.Data) -> UIView {
        ImageView.nibbed() ?? UIView()
    }

    func bind(_ data: Item.Data, _ view: UIView, _ version: Version, _ invalidate: @escaping Invalidate) {
        guard case let .image(image) = data, let view = view as? ImageView else { return }

        print(version)

        // this is a bit broke at the moment, trying to get dynamically updating heights
        // and also tracking when an image has loaded by incrementing a version number;
        // doesn't quite work...

        // also, really, it'd be nice to dynamically add support for types without needing
        // to edit a central lookup / enum / model thing, that needs a think...

        if let placeholder = UIImage(named: "Placeholder") {
            view.content.image   = placeholder
            view.height.constant = round(view.bounds.size.width / placeholder.size.aspectRatio)
        }
//        print("A -> \(view.height.constant)")

        URLSession.shared.dataTask(with: image.value) { data, response, error in
            guard let data = data, let image = UIImage(data: data) else { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
//                let height = round(view.bounds.size.width / image.size.aspectRatio)
                view.animate {
                    view.content.image = image
                } completion: { _ in
//                    view.height.constant = height
//                    print("B -> \(view.height.constant)")
//                    invalidate(version + 1)
                }
            }
        }.resume()
    }
}

class ImageView: UIView {
    @IBOutlet weak var content: UIImageView!
    @IBOutlet weak var height: NSLayoutConstraint!
}
